const battery = await Service.import("battery");

const icon = Utils.merge([battery.bind("percent"), battery.bind("charging")], (p, c) => {
  return `battery-level-${Math.floor(p / 10) * 10}${c ? '-charging' : ''}-symbolic`;
})


export default () => Widget.Box({
  className: "battery-indicator",
  visible: battery.bind("available"),
  children: [
    Widget.Icon({
      className: "battery-icon",
      icon: icon
    }),
    Widget.Label({
      label: battery.bind("percent").as(p => `${Math.floor(p)}`)
    }),
  ],
});
