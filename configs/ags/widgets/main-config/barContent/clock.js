import GLib from 'gi://GLib';

const { Button, Label } = Widget;

const interval = 1000 * 60;
const format = '%a %d %b, %H:%M';

const Time = () => {
  const self = Label({ classNames: ["bar-clock-time"] })
  Utils.interval(interval, () => {
    self
      .label = GLib.DateTime.new_now_local().format(format);
  },
    self)
  return self;

}


export default () => Button({
  cursor: 'pointer',
  on_clicked: () => console.log('clock'), // ClickeD
  classNames: ["bar-clock", "bar-content"],
  child: Time(),
});
