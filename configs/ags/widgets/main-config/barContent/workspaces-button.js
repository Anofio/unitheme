import Hyprland from 'resource:///com/github/Aylur/ags/service/hyprland.js';
const { Button, Box } = Widget;

const dispatch = ws => Hyprland.messageAsync(`dispatch workspace ${ws}`);


export const WorkspaceButton = (id) =>
  Button({
    cursor: "pointer",
    on_clicked: () => dispatch(id),
    classNames: ["workspace-button-wrapper"],
    child: Box({
      classNames: ["workspace-button", "hort"],
      hexpand: false,
    }),
    setup: (self) => {
      self.hook(Hyprland, () => {
        var workspaces = Hyprland.workspaces.sort((a, b) => a.id - b.id);
        const last = workspaces[workspaces.length - 1].id;
        self.visible = id <= last || id === last + 1;
        self.toggleClassName("active", Hyprland.active.workspace.id == id);
        self.toggleClassName("occupied", Hyprland.getWorkspace(id)?.windows > 0);
      })
    }
  });


export default () => Box({
  child: Box({
    classNames: ["workspace-group"],
    children: Array.from({ length: 9 }, (_, i) => i + 1).map(i => WorkspaceButton(i))
  })
});

