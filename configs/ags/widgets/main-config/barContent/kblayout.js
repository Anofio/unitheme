import Icons from "../../assets/icons.js";
import Hyprland from 'resource:///com/github/Aylur/ags/service/hyprland.js';

import { exec } from 'resource:///com/github/Aylur/ags/utils.js';

const getLayout = () => {
  const devices = JSON.parse(exec('hyprctl -j devices'));
  const keyboard = devices.keyboards.find(k => k.name === 'at-translated-set-2-keyboard');
  const activeKeymap = keyboard.active_keymap;
  const layout = activeKeymap.slice(0, 2).toLowerCase();
  return layout;
};

export default () =>
  Widget.Box({
    classNames: ["keyboardlayout", "bar-content"],
    spacing: 3,
    children: [
      Widget.Icon({
        className: "keyboardlayout-icon",
        icon: Icons.keyboard.language,
      }),
      Widget.Label({
        className: "keyboardlayout-label",
        label: Utils.watch(getLayout(), Hyprland, "keyboard-layout", (_) => getLayout()),
      }),
    ]
  });
