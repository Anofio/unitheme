import Hyprland from 'resource:///com/github/Aylur/ags/service/hyprland.js';
import WorkspaceButtonGroup from "./workspaces-button.js";


const setWs = ws => Hyprland.messageAsync(`dispatch workspace ${ws}`);
const dispatch = (direction) => {
  const active = Hyprland.active.workspace.id;
  const workspaces = Hyprland.workspaces.sort((a, b) => a.id - b.id);
  const last = workspaces[workspaces.length - 1].id;

  if (active === last && direction === "+1") {
    setWs('1');
  } else if (active === 1 && direction === "-1") {
    setWs(last);
  } else {
    setWs(direction)
  }
};

export default () => Widget.EventBox({
  classNames: ["workspaces", "bar-content"],
  onScrollUp: () => dispatch('+1'),
  onScrollDown: () => dispatch('-1'),
  child: WorkspaceButtonGroup()
});

