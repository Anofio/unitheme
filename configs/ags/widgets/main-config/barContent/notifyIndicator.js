import Icons from "../../assets/icons.js"


const notifications = await Service.import("notifications");


export default () => Widget.Button({
  className: "notification-indicator",
  onClicked: () => {
    App.toggleWindow("keygrab-" + "notification-list".toUpperCase()) // hyprland sucks
    App.toggleWindow("notification-list")
  },
  child: Widget.Box({
    spacing: 4,
    children: [
      Widget.Icon({ icon: Icons.quicksettings.notifications }),
      Widget.Label({
        label: notifications.bind("notifications").as(n =>
          `${n.length}`
        ),
      }),

    ]
  })
})
