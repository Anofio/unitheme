import Battery from "./battery.js";
import Volume from "./volume.js";

export default () => Widget.Button({
  classNames: ["quickaccess-wrapper", "bar-content"],
  onClicked: () => {
    App.toggleWindow("keygrab-" + "controlcenter".toUpperCase()) // hyprland sucks
    App.toggleWindow("controlcenter")
  },
  child: Widget.Box({
    vpack: "center",
    classNames: ["quickaccess"],
    spacing: 10,
    children: [
      Volume(),
      Battery(),
    ]
  })
});
