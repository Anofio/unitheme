const audio = await Service.import("audio");

const icons = {
  101: "overamplified",
  67: "high",
  34: "medium",
  1: "low",
  0: "muted",
};

const getIcon = () => {
  const icon = audio.speaker.is_muted ? 0 : [101, 67, 34, 1, 0].find(
    threshold => threshold <= audio.speaker.volume * 100)
  return `audio-volume-${icons[icon]}-symbolic`
};

const icon = Widget.Icon({
  icon: Utils.watch(getIcon(), audio.speaker, getIcon),
});

const percent = Widget.Label({
  setup: self => self.hook(audio.speaker, () => {
    const volume = Math.round(audio.speaker.volume * 100);
    self.label = (volume && !audio.speaker.is_muted) ? volume.toString() + "%" : "";
  }),
});

export default () => Widget.Box({
  classNames: ["volume-indicator"],
  children: [
    icon,
    percent
  ],
  setup: self => self.hook(audio.speaker, () => {
    self.value = audio.speaker.volume || 0
  }),
});
