import Notification from "./notification.js"


const notifications = await Service.import("notifications")


export default () => Widget.Scrollable({
  className: "notification-list-inner",
  hscroll: 'never',
  vscroll: 'always',
  child: Widget.Box({
    spacing: 10,
    vertical: true,
    children: notifications.bind('notifications')
      .as(notifs => notifs.length ?
        notifs.map(Notification).reverse().slice(0, 10) :
        [
          Widget.Label({
            classNames: ["notification-title", "notification-title-empty"],
            label: "No notifications"
          }),
        ]),
  }),
})


