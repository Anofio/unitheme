import Icons from "../../assets/icons.js"


const notifications = await Service.import("notifications")

const DNDToggle = () => {
  notifications.dnd = !notifications.dnd
}


export default () => Widget.Button({
  className: "dnd-button-icon",
  on_clicked: () => { DNDToggle() },
  child: Widget.Icon({
    icon: notifications.bind('dnd').as(e => e ?
      Icons.notifications.silent :
      Icons.notifications.noisy),
  })
})
