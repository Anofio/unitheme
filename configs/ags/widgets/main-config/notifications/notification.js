import GLib from 'gi://GLib';
import Pango from 'gi://Pango';
import Icons from "../../assets/icons.js";

import {
  lookUpIcon
} from "resource:///com/github/Aylur/ags/utils.js";


const {
  Box,
  Icon,
  Label,
  ProgressBar,
  EventBox,
  Button
} = Widget;

const NotificationIcon = notification => {
  let icon;
  if (notification.image) {
    return Box({
      vexpand: false,
      hexpand: false,
      vpack: "center",
      class_name: "notification-icon",
      css: `background-image: url('${notification.image}');
                  background-size: auto 100%;
                  background-repeat: no-repeat;
                  background-position: center;`,
    });
  } else if (lookUpIcon(notification.app_icon)) icon = notification.app_icon;
  else icon = Icons.notifications.chat;
  return Icon({
    class_name: "notification-icon",
    icon: icon
  });
};

const NotificationTitle = notification => {
  return Label({
    class_name: "notification-title",
    xalign: 0,
    justification: "left",
    hexpand: true,
    max_width_chars: 24,
    truncate: "end",
    wrap: true,
    label: notification.summary.substring(0, 31),
    use_markup: true,
  })
}

const NotificationTime = notification => {
  return Label({
    className: "notification-time",
    label: GLib.DateTime.new_from_unix_local(notification.time).format("%H:%M"),
  })
}

const NotificationCloseButton = notification => {
  return Button({
    className: "notification-close-button",
    child: Icon({
      icon: Icons.notifications.close,
      className: "notification-close-button-icon"
    }),
    onClicked: () => {
      notification.close(), notification.dismiss()
    },
  })
}

const NotificationBody = notification => {
  return Label({
    className: "notification-body",
    justification: "left",
    max_width_chars: 24,
    lines: 3,
    truncate: "end",
    wrap: true,
    xalign: 0,
    wrap_mode: Pango.WrapMode.WORD_CHAR,
    label: notification.body.replace(/(\r\n|\n|\r|<b>|<\/b>|<i>|<\/i>)/gm, " ")

  })
}

const NotificationProgress = notification => {
  return notification.hints.value ? ProgressBar({
    value: Number(notification.hints.value.unpack()) / 100
  }) : Box()
}

const Actions = (n) => Widget.Box({
  class_name: "notification-actions",
  children: n.actions.map(({
    id,
    label
  }) => Widget.Button({
    class_name: "notification-action-button",
    on_clicked: () => {
      n.invoke(id)
      // n.dismiss()
    },
    hexpand: true,
    child: Widget.Label(label),
  })),
})

const icon = (n) => Widget.Box({
  vpack: "center",
  class_name: "notification-icon",
  child: NotificationIcon(n),
})


export default (n) => EventBox({
  attribute: {
    id: n.id
  },
  on_primary_click: n.dismiss,
},
  Widget.Box({
    class_name: `notification ${n.urgency}`,
    vertical: false,
  },
    Widget.Box([
      icon(n),
      Box({
        vertical: true
      },
        Box(
          Label({
            class_name: "notification-separator"
          }),
          NotificationTitle(n),
          NotificationTime(n),
          NotificationCloseButton(n),
        ),
        NotificationBody(n),
        NotificationProgress(n),
        Actions(n),
      ),
    ]),
  ),
)
