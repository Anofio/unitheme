import Icons from "../../assets/icons.js"


const notifications = await Service.import("notifications")

const ClearAll = () => {
  notifications.clear()
}


export default () => Widget.Overlay({
  child: Widget.Button({
    className: "clear-all-button-label",
    on_clicked: () => { ClearAll() },
    child: Widget.Icon({
      icon: Icons.trash.empty,
    })
  })
})

