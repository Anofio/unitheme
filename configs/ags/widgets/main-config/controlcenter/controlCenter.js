import Media from "./media.js";
import Bluetooth from "./bluetooth.js";


// export default () => Widget.Box({
//   className: "controlcenter-inner",
//   vertical: true,
//   children: [
//     Widget.Box({
//       vertical: false,
//       children: [
//         Bluetooth(),
//         Widget.Label({
//           className: "wifi-card",
//           label: "Wifi"
//         }),
//       ]
//     }),

//     Media(),
//   ],
// });

const top = Widget.CenterBox({
  startWidget: Bluetooth(),
  endWidget: Widget.Label({ label: "Wifi" }),
})

const bottom = Media()

export default () => Widget.Box({
  className: "controlcenter-inner",
  vertical: true,
  children: [
    top,
    bottom
  ],
});
