const bluetooth = await Service.import('bluetooth')

const connectedList = Widget.Box({
  className: 'bluetooth-connected-list',
  setup: self => self.hook(bluetooth, self => {
    self.children = bluetooth.connected_devices
      .map(({ icon_name, name }) => Widget.Box([
        Widget.Icon(icon_name + '-symbolic'),
        Widget.Label(name),
      ]));

  }, 'notify::connected-devices'),
})

const indicator = Widget.Icon({
  className: 'bluetooth-indicator',
  icon: bluetooth.bind('enabled').as(on =>
    `bluetooth-${on ? 'active' : 'disabled'}-symbolic`),
})


export default () => Widget.Box({
  vertical: true,
  children: [
    Widget.CenterBox({
      className: 'bluetooth-card',
      centerWidget: Widget.Box({
        child: indicator,
      })
    }),
    // Widget.CenterBox({
    //   className: 'bluetooth-card',
    //   centerWidget: Widget.Box({

    //     child: connectedList,
    //   })
    // })

  ]
})
