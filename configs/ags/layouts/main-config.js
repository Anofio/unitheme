App.addIcons(`${App.configDir}/assets`)


import Notifications from "resource:///com/github/Aylur/ags/service/notifications.js";
import Bar from "./windows/bar.js";
import ControlCenterWindow, { ControlCenterWindowKeygrabber } from "./windows/controlCenterWindow.js";
import NotificationMenu, { NotificationMenuKeygrabber } from "./windows/notificationMenu.js";
import PopupNotifications from "./windows/popupNotification.js";


const scss = `${App.configDir}/styles/style.scss`
const css = `/tmp/ags-style.css`

Utils.exec(`sassc ${scss} ${css}`)


Notifications.popupTimeout = 5000;
Notifications.forceTimeout = true;
Notifications.clearDelay = 100;


App.config({
  style: css,
  windows: [
    Bar(),
    PopupNotifications(),
    NotificationMenuKeygrabber(),
    NotificationMenu(),
    ControlCenterWindowKeygrabber(),
    ControlCenterWindow(),
  ],
  // onConfigParsed: onConfigParsed
})


Utils.monitorFile(
  `${App.configDir}/styles`,
  function() {
    const scss = `${App.configDir}/styles/style.scss`
    const css = `/tmp/ags-style.css`
    Utils.exec(`sassc ${scss} ${css}`)
    App.resetCss()
    App.applyCss(css)
  },
)


export { }
