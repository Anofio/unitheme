import App from 'resource:///com/github/Aylur/ags/app.js';
import Widget from 'resource:///com/github/Aylur/ags/widget.js';
import Service from 'resource:///com/github/Aylur/ags/service.js';
import Variable from 'resource:///com/github/Aylur/ags/variable.js';
import * as Utils from 'resource:///com/github/Aylur/ags/utils.js';

import Applications from 'resource:///com/github/Aylur/ags/service/applications.js';
import Audio from 'resource:///com/github/Aylur/ags/service/audio.js';
import Battery from 'resource:///com/github/Aylur/ags/service/battery.js';
import Bluetooth from 'resource:///com/github/Aylur/ags/service/bluetooth.js';
import Hyprland from 'resource:///com/github/Aylur/ags/service/hyprland.js';
import Mpris from 'resource:///com/github/Aylur/ags/service/mpris.js';
import Network from 'resource:///com/github/Aylur/ags/service/network.js';
import SystemTray from 'resource:///com/github/Aylur/ags/service/systemtray.js';
import Notifications from "resource:///com/github/Aylur/ags/service/notifications.js";

// import Bar from './windows/bar.js'
// import { PopupNotifications } from './widgets/notifications/_init.js'
// import { NotificationPopups } from './windows/notifyWindow.js'
// import NotificationMenu from './windows/notifyMenu.js'

// const Windows = [
//   Bar(),
//   PopupNotifications,
//   // NotificationPopups(),
//   // NotificationMenu(),
// ]


const { Icon, Box, Label, Button, EventBox, Revealer, ProgressBar } = Widget;

export {
  App,
  Widget,
  Service,
  Variable,
  Utils,
  Applications,
  Audio,
  Battery,
  Bluetooth,
  Hyprland,
  Mpris,
  Network,
  SystemTray,
  Notifications,
  Box,
  Label,
  Icon,
  Button,
  EventBox,
  Revealer,
  ProgressBar,
}
