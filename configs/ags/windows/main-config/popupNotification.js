import Notification from "../widgets/notifications/notification.js"


const notifications = await Service.import("notifications")

const list = Widget.Box({
  vertical: true,
  spacing: 10,
  children: notifications.bind('popups')
    .as(popups => popups.map(Notification).slice(0, 3))
})


export default () => Widget.Window({
  name: `notifications`,
  class_name: "notification-popups",
  anchor: ["bottom", "left"],
  margins: [0, 10, 10, 10],
  layer: "overlay",
  visible: notifications.bind('popups').as(popups => popups.length > 0),
  child: Widget.Box({
    css: "min-width: 2px; min-height: 2px;",
    class_name: "notifications",
    vertical: true,
    child: list,
  }),
})
