export default (windowname) => Widget.Window({
  name: `keygrab-${windowname.toUpperCase()}`,
  anchor: ["top", "right", "bottom", "left"],
  visible: false,
  css: "background: none; background-color: transparent;",
  child: Widget.EventBox({
    onPrimaryClick: () => {
      App.toggleWindow(windowname)
      App.toggleWindow("keygrab-" + windowname.toUpperCase())
    }
  })
})

