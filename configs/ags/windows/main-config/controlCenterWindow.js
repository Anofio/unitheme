import ControlCenter from "../widgets/controlcenter/controlCenter.js"
import KeygrabWindow from "./keygrabber.js"


export default () => Widget.Window({
  name: "controlcenter",
  className: "controlcenter",
  anchor: ["bottom", "right"],
  margins: [0, 10, 10, 0],
  visible: false,
  keymode: "on-demand",
  child: Widget.Box({
    children: [
      ControlCenter(),
      // Widget.Button().on("key-press-event", (self, event) => {
      // print(event.key)
      // check event for keys
      // })
      // Widget.Button().keybind('Mod1+p', self => { print(1) })
    ]
  })
})


export const ControlCenterWindowKeygrabber = () => KeygrabWindow("controlcenter")
