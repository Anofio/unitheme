import Workspaces from '../widgets/barContent/workspaces.js'
import Clock from '../widgets/barContent/clock.js'
import NotifyIndicator from '../widgets/barContent/notifyIndicator.js'
import QuickAccess from '../widgets/barContent/quickAccess.js'
import SysTray from '../widgets/barContent/systray.js'
import KbLayout from '../widgets/barContent/kblayout.js'



const Left = () => Widget.Box({
  spacing: 8,
  children: [
    Workspaces(),
  ],
})

const Center = () => Widget.Box({
  spacing: 6,
  children: [
    Clock(),
    NotifyIndicator(),
  ],
})

const Right = () => Widget.Box({
  hpack: "end",
  spacing: 6,
  children: [
    SysTray(),
    KbLayout(),
    QuickAccess(),
  ],
})


export default (gdkmonitor) => Widget.Window({
  gdkmonitor,
  name: `bar-${gdkmonitor}`, // name has to be unique
  class_name: "bar",
  anchor: ["bottom", "left", "right"],
  margins: [0, (1920 - 1100) / 2, 5, (1920 - 1100) / 2],
  // margins: [10, 10, 0, 10],
  exclusivity: "exclusive",
  child: Widget.CenterBox({
    className: "bar-inner",
    start_widget: Left(),
    center_widget: Center(),
    end_widget: Right(),
  }),
})
