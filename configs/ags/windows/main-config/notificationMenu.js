import NotificationList from "../widgets/notifications/notificationList.js"
import DNDButton from "../widgets/notifications/dnd.js"
import ClearAllButton from "../widgets/notifications/clearAllButton.js"
import KeygrabWindow from "./keygrabber.js";


export default () => Widget.Window({
  name: `notification-list`,
  class_name: "notification-list",
  anchor: ["bottom"],
  margins: [0, 0, 10, 0],
  visible: false,
  child: Widget.Box({
    class_name: "notifications",
    child: Widget.Box({
      vertical: true,
      children: [
        Widget.CenterBox({
          spacing: 40,
          className: "DNDControls",
          vexpand: true,
          vpack: "center",
          startWidget: Widget.Box({
            hpack: "start",
            child: Widget.Label({ classNames: ["notification-title", "notification-menu-main-title"], label: "Notifications" })
          }),
          endWidget: Widget.Box({
            hpack: "end",
            children: [
              ClearAllButton(),
              DNDButton(),
            ]
          }),
        }),
        NotificationList(),
      ]
    }),
  }),
})


export const NotificationMenuKeygrabber = () => KeygrabWindow("notification-list")
