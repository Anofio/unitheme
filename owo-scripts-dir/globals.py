from os import environ, getenv, mkdir

VERSION = "0.1.0"

HOME_DIR = getenv("HOME")
UNITHEME_DIR = getenv("UNITHEME_DIR")
CONFIG_DIR = getenv("XDG_CONFIG_HOME")


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


if not HOME_DIR:
    raise FileNotFoundError

if not CONFIG_DIR:
    CONFIG_DIR = f"{HOME_DIR}/.config"


DEFAULT_UNITHEME_DIR = f"{HOME_DIR}/.local/share/unitheme"

if not UNITHEME_DIR:
    mkdir(DEFAULT_UNITHEME_DIR)
    environ["UNITHEME_DIR"] = DEFAULT_UNITHEME_DIR
