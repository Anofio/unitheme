# rofi
def app(args):
    return [
        {
            "colors": ("colors", "rasi"),
            "layouts": ("config", "rasi"),
            "config_path": "rofi",
            "link_to_config_dir": True,
        },
        {
            "colors": args["--theme"],
            "layouts": args["--layout"],
        },
        {
            "colors": "Set with -t",
            "layouts": "Set with -l",
        },
    ]
