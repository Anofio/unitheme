# waybar
def app(args):
    return [
        {
            "layouts": ("config", "jsonc"),
            "colors": ("colors", "css"),
            "styles": ("style", "css"),
            "config_path": "waybar",
            "final": "pkill waybar && waybar &",
            "link_to_config_dir": True,
        },
        {
            "layouts": args["--layout"],
            "styles": args["--layout"],
            "colors": args["--theme"],
        },
        {
            "layouts": "set with -l",
            "styles": "linked with layouts",
            "colors": "set with -t",
        },
    ]
