"""
Dir:
$UNITHEME_DIR/configs/app_name
├── colors
│   ├── theme1.ext1
│   └── theme2.ext1
├── current
│   ├── color.ext1    -> ../colors/theme1.ext1
│   ├── bg-image.ext2 -> ../images/image1.ext2
│   └── config.ext3   -> ../layouts/layout1.ext3
├── images
│   ├── image1.ext2
│   └── image2.ext2
└── layouts
    └── layout1.ext3

Function:
def app(args: dict) -> tuple[dict, dict, dict]:
    return (
        {
            # How to link to files
            "colors": ("color", "ext1"),
            "images": ("image", "ext2"),
            "layouts": ("config", "ext3"),
            # Config path (no need to write /home/user/.config)
            "config_path": "app_name",
            "link_to_config_dir": True, # Set False if no config_path
            # Final hook if needed
            "final": "pkill app_name && app_name &",
        },
        {
            # Which arg to use for config
            "colors": args["--theme"],
            "images": args["--image"],
            "layouts": args["--layout"],
        },
        {
            # Hints in show menu (-s)
            "colors": "Set with -t",
            "images": "Set with -i",
            "layouts": "Set with -l",
        },
    )
"""
