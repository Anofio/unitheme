def app(args):
    return [
        {
            "overrides": ("overrides", "conf"),
            "borders": ("borders", "conf"),
            "config_path": "hypr",
            "link_to_config_dir": True,
        },
        {
            "borders": args["--theme"],
            "overrides": args["--layout"],
        },
        {
            "borders": "Set with -t",
            "overrides": "Set with -l",
        },
    ]
