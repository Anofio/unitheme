# firefox
def app(args: dict) -> tuple[dict, dict, dict]:
    return (
        {
            "colors": ("colors", "css"),
            "layouts": ("userChrome", "css"),
            "link_to_config_dir": False,
        },
        {
            "layouts": args["--layout"],
            "colors": args["--theme"],
        },
        {
            "layouts": "Set with -l",
            "colors": "Set with -t",
        },
    )
