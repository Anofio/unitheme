"""
owo-theme-set

Usage:
    owo-theme-set <app> [options]

Options:
    -h --help             Show this screen.
    -v --version          Show version.
    -l --layout <layout>   Set layout.
    -t --theme <theme>     Set theme.
    -i --image <image>     Set image if supported.
    -s --show             Show all available configs.
"""

import textwrap
from glob import glob
from os import makedirs, path, symlink, unlink
from subprocess import DEVNULL, run
from typing import Dict, List

from docopt import docopt
from globals import CONFIG_DIR, UNITHEME_DIR, VERSION, bcolors


def print_oneline(key: str, value: str) -> None:
    """
    Prints a key-value pair in a formatted manner.

    Args:
        key (str): The key to be printed.
        value (str): The value to be printed.
    """
    wrapped_value = textwrap.fill(value, width=20, subsequent_indent=" " * 10)
    print(
        f"{bcolors.BOLD}{bcolors.OKGREEN}{key:<15}{bcolors.ENDC} "
        f"{bcolors.HEADER}{bcolors.UNDERLINE}{wrapped_value}{bcolors.ENDC}"
    )


def force_symlink(file: str, dest_dir: str, dest_file: str) -> int:
    """
    Creates a symbolic link from 'file' to 'dest_dir/dest_file'.

    Args:
        file (str): The source file path.
        dest_dir (str): The destination directory path.
        dest_file (str): The destination file name.

    Returns:
        int: 0 on success, 1 on failure.
    """
    if not path.exists(file):
        print(f"{bcolors.FAIL}No such file: {file}{bcolors.ENDC}")
        return 1

    if not path.exists(dest_dir):
        makedirs(dest_dir)

    if path.exists(f"{dest_dir}/{dest_file}"):
        unlink(f"{dest_dir}/{dest_file}")

    symlink(file, f"{dest_dir}/{dest_file}")
    return 0


def link_config(
    app_name: str,
    app_config: Dict[str, str],
    config_type: str,
    config_name: str,
    link_to_config_dir: bool = False,
) -> int:
    """
    Creates symbolic links for the specified configuration.

    Args:
        app_name (str): The application name.
        app_config (Dict[str, str]): The application configuration dictionary.
        config_type (str): The configuration type (layout, theme, image).
        config_name (str): The configuration name.
        link_to_config_dir (bool): Whether to link to the config directory.

    Returns:
        int: 0 on success, 1 on failure.
    """
    ext = app_config[config_type][1]
    file = f"{UNITHEME_DIR}/configs/{app_name}/{config_type}/{config_name}.{ext}"
    out_file = f"{app_config[config_type][0]}.{ext}"
    dest = f"{UNITHEME_DIR}/configs/{app_name}/current"
    err = force_symlink(file, dest, out_file)

    if link_to_config_dir and not err:
        dest_config = f"{CONFIG_DIR}/{app_config['config_path']}"
        err = force_symlink(file, dest_config, out_file)

    return err


def apply_app(app_name: str, app_config: Dict, **kwargs) -> None:
    """
    Applies the specified configuration for the given application.

    Args:
        app_name (str): The application name.
        app_config (Dict): The application configuration dictionary.
        **kwargs: The configuration options (layout, theme, image, etc).
    """
    for prop in kwargs.keys():
        if kwargs[prop]:
            err = link_config(
                app_name,
                app_config,
                prop,
                kwargs[prop],
                app_config["link_to_config_dir"],
            )

            if err:
                print(f"{bcolors.FAIL}Failed to apply config{bcolors.ENDC}")
                return

            print(
                f"{bcolors.OKBLUE}{app_name.capitalize()}: apply {prop} {kwargs[prop]}{bcolors.ENDC}"
            )
    if "final" in app_config.keys():
        run(app_config["final"], shell=True, stdout=DEVNULL)


def show_configs(app_name: str, hints: dict) -> None:
    """
    Shows all available configurations for the given application.

    Args:
        app_name (str): The application name.
        hints (dict): The hints for each configuration type.
    """
    dirs = [dir.split("/")[-2] for dir in glob(f"{UNITHEME_DIR}/configs/{app_name}/*/")]
    for dir in dirs:
        if dir == "current":
            continue

        print(f"{bcolors.HEADER}{dir} ({hints[dir]}) {bcolors.ENDC}")
        for file in glob(f"{UNITHEME_DIR}/configs/{app_name}/{dir}/*"):
            print(
                f"  - {bcolors.OKGREEN}{file.split('/')[-1].split('.')[0]}{bcolors.ENDC}"
            )  # Extract filename without extension


def main() -> None:
    """
    Main function that handles command-line arguments and calls the appropriate.
    """
    args = docopt(__doc__, version=VERSION)
    app = getattr(
        __import__(f"apps.{args['<app>'].lower()}"), args["<app>"].lower()
    ).app(args)

    if args["--show"]:
        show_configs(args["<app>"].lower(), app[2])
        return

    if not any([args["--layout"], args["--theme"], args["--image"]]) and args["<app>"]:
        print(
            f"{bcolors.WARNING}{args["<app>"].capitalize()}: Nothing to do{bcolors.ENDC}"
        )
        return

    apply_app(
        args["<app>"].lower(),
        app[0],
        **app[1],
    )


if __name__ == "__main__":
    main()
