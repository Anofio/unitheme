import textwrap
from glob import glob
from json import dump, load
from subprocess import DEVNULL, run
from typing import Dict

from owo_scripts.globals import (CONFIG_DIR, UNITHEME_DIR, bcolors,
                                 force_symlink, get_apps, load_config)
from owo_scripts.wallpaper import main as set_wallpaper


def print_oneline(key: str, value: str) -> None:
    """
    Prints a key-value pair in a formatted manner.

    Args:
        key (str): The key to be printed.
        value (str): The value to be printed.
    """
    wrapped_value = textwrap.fill(value, width=20, subsequent_indent=" " * 10)
    print(
        f"{bcolors.BOLD}{bcolors.OKGREEN}{key:<15}{bcolors.ENDC} "
        f"{bcolors.HEADER}{bcolors.UNDERLINE}{wrapped_value}{bcolors.ENDC}"
    )


def link_config(
    app_name: str,
    app_config: Dict[str, str],
    config_type: str,
    config_name: str,
) -> int:
    """
    Creates symbolic links for the specified configuration.

    Args:
        app_name (str): The application name.
        app_config (Dict[str, str]): The application configuration dictionary.
        config_type (str): The configuration type (layout, theme, image).
        config_name (str): The configuration name.
        link_to_config_dir (bool): Whether to link to the config directory.

    Returns:
        int: 0 on success, 1 on failure.
    """
    ext = app_config[config_type][1]
    file_base = f"{UNITHEME_DIR}/configs/{app_name}/{config_type}/{config_name}"
    file = f"{file_base}.{ext}" if ext != "dir" else f"{file_base}"
    out_file = (
        f"{app_config[config_type][0]}.{ext}"
        if ext != "dir"
        else f"{app_config[config_type][0]}"
    )
    dest = f"{UNITHEME_DIR}/configs/{app_name}/current"
    err = force_symlink(file, dest, out_file)

    if app_config["link_to_config_dir"] and not err:
        dest_config = f"{CONFIG_DIR}/{app_config['config_path']}"
        err = force_symlink(file, dest_config, out_file)

    return err


def apply_app(app_name: str, app_config: Dict, **kwargs) -> None:
    """
    Applies the specified configuration for the given application.

    Args:
        app_name (str): The application name.
        app_config (Dict): The application configuration dictionary.
        **kwargs: The configuration options (layout, theme, image, etc).
    """
    if "pre" in app_config.keys():
        run(app_config["pre"], shell=True, stdout=DEVNULL)
    for prop in kwargs.keys():
        if kwargs[prop] and prop != "hints":
            err = link_config(
                app_name,
                app_config,
                prop,
                kwargs[prop],
            )

            if err:
                show_configs(app_name, kwargs["hints"])
                print(f"{bcolors.FAIL}Exiting...{bcolors.ENDC}")
                return

            print(
                f"{bcolors.OKBLUE}{app_name.capitalize()}: apply {prop} {kwargs[prop]}{bcolors.ENDC}"
            )
    if "final" in app_config.keys() and app_config["final"]:
        run(app_config["final"], shell=True, stdout=DEVNULL)


def show_configs(app_name: str, hints: dict) -> None:
    """
    Shows all available configurations for the given application.

    Args:
        app_name (str): The application name.
        hints (dict): The hints for each configuration type.
    """
    dirs = [dir.split("/")[-2] for dir in glob(f"{UNITHEME_DIR}/configs/{app_name}/*/")]
    for dir in dirs:
        if dir == "current" or "linked" in hints[dir]:
            continue

        print(f"{bcolors.HEADER}{dir} ({hints[dir]}) {bcolors.ENDC}")
        for file in glob(f"{UNITHEME_DIR}/configs/{app_name}/{dir}/*"):
            print(
                f"  - {bcolors.OKGREEN}{file.split('/')[-1].split('.')[0]}{bcolors.ENDC}"
            )  # Extract filename without extension


def apply_config(app_name: str, args: dict) -> None:
    app = load_config(app_name, "config")
    if app:
        app = app.app(args)
    else:
        print(f"{bcolors.FAIL}Exiting...{bcolors.ENDC}")
        return

    if args["--show"]:
        show_configs(app_name, app[2])
        return

    if not any([args["--layout"], args["--image"]]) and app_name:
        print(f"{bcolors.WARNING}{app_name.capitalize()}: Nothing to do{bcolors.ENDC}")
        return

    apply_app(
        app_name,
        app[0],
        **app[1],
        hints=app[2],
    )


def main(args) -> None:
    """
    Main function that handles command-line arguments.
    """
    with open(f"{UNITHEME_DIR}/info.json", "r") as f:
        table = load(f)
        if args["<theme>"]:
            with open(f"{UNITHEME_DIR}/info.json", "w") as f:
                table["theme"] = args["<theme>"]
                dump(table, f, indent=4)
            set_wallpaper()
            if table["whitelist"]:
                for app in table["whitelist"]:
                    apply_config(app, args)
            elif table["blacklist"]:
                for app in [app for app in get_apps() if app not in table["blacklist"]]:
                    apply_config(app, args)
            else:
                for app in get_apps():
                    apply_config(app, args)
        else:
            app_name = args["<app>"].lower()
            apply_config(app_name, args)


if __name__ == "__main__":
    print(f"{bcolors.WARNING}Do not run this script directly.{bcolors.ENDC}")
