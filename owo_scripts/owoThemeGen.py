from glob import glob
from json import load
from subprocess import DEVNULL, run

from owo_scripts.globals import UNITHEME_DIR, bcolors, get_apps, load_config


def gen_pallete(app: str, where: str) -> str | None:
    """
    Loads the config and generates the pallete

    Args:
        app (str): The app name
        theme (str): The theme name
    """
    pallete_text = []
    pallete = load_config(app, "pallete")
    if pallete:
        try:
            for func in pallete.funcs:
                pallete_text.append(func(where))
        except KeyError:
            print(f"Check config for {app}!")
            return
    else:
        print(f"{bcolors.FAIL}Exiting...{bcolors.ENDC}")
        return
    return "".join(pallete_text)


def create_colors_for_app(app_name: str, args: dict, where: str = "") -> None:
    app_config = load_config(app_name, "config")
    if app_config:
        app_config = app_config.app(args)[0]
        if app_config and "final" in app_config:
            run(app_config["final"], shell=True, stdout=DEVNULL)
    if not where:
        where = f"{UNITHEME_DIR}/colors/_current/matugen.json"
    pallete = gen_pallete(app_name, where)
    if pallete and app_config:
        print(
            f"{bcolors.HEADER}Generating colors for {app_name.capitalize()}{bcolors.ENDC}"
        )
        file_name = f"{app_config['colors'][0]}.{app_config['colors'][1]}"
        with open(
            f"{UNITHEME_DIR}/configs/{app_name}/current/{file_name}",
            "w",
        ) as color_file:
            color_file.write(pallete)
    return


def main(args):
    """
    Main function that handle command-line arguments.
    """
    if args["--show"]:
        print(f"{bcolors.HEADER}Colors{bcolors.ENDC}")
        for file in glob(f"{UNITHEME_DIR}/colors/*.conf"):
            print(
                f"  - {bcolors.OKBLUE}{file.split("/")[-1].split(".")[0]}{bcolors.ENDC}"
            )
        return

    app_name = args["<app>"]
    if app_name == "all":
        for app in get_apps():
            create_colors_for_app(app, args)
    else:
        create_colors_for_app(app_name, args)


if __name__ == "__main__":
    print(f"{bcolors.WARNING}Do not run this script directly.{bcolors.ENDC}")
