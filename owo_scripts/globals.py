from glob import glob
from json import load
from os import environ, getenv, makedirs, mkdir, path, symlink, unlink
from subprocess import run
from typing import Dict, List

VERSION = "0.1.0"

HOME_DIR = getenv("HOME")
UNITHEME_DIR = getenv("UNITHEME_DIR")
CONFIG_DIR = getenv("XDG_CONFIG_HOME")


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


if not HOME_DIR:
    raise FileNotFoundError

if not CONFIG_DIR:
    CONFIG_DIR = f"{HOME_DIR}/.config"


DEFAULT_UNITHEME_DIR = f"{HOME_DIR}/.local/share/unitheme"


if not UNITHEME_DIR:
    mkdir(DEFAULT_UNITHEME_DIR)
    environ["UNITHEME_DIR"] = DEFAULT_UNITHEME_DIR

DEFAULT_COLORMAP = {
    "background": "background",
    "error": "error",
    "error_container": "error_container",
    "inverse_on_surface": "inverse_on_surface",
    "inverse_primary": "inverse_primary",
    "inverse_surface": "inverse_surface",
    "on_background": "on_background",
    "on_error": "on_error",
    "on_error_container": "on_error_container",
    "on_primary": "on_primary",
    "on_primary_container": "on_primary_container",
    "on_primary_fixed": "on_primary_fixed",
    "on_primary_fixed_variant": "on_primary_fixed_variant",
    "on_secondary": "on_secondary",
    "on_secondary_container": "on_secondary_container",
    "on_secondary_fixed": "on_secondary_fixed",
    "on_secondary_fixed_variant": "on_secondary_fixed_variant",
    "on_surface": "on_surface",
    "on_surface_variant": "on_surface_variant",
    "on_tertiary": "on_tertiary",
    "on_tertiary_container": "on_tertiary_container",
    "on_tertiary_fixed": "on_tertiary_fixed",
    "on_tertiary_fixed_variant": "on_tertiary_fixed_variant",
    "outline": "outline",
    "outline_variant": "outline_variant",
    "primary": "primary",
    "primary_container": "primary_container",
    "primary_fixed": "primary_fixed",
    "primary_fixed_dim": "primary_fixed_dim",
    "scrim": "scrim",
    "secondary": "secondary",
    "secondary_container": "secondary_container",
    "secondary_fixed": "secondary_fixed",
    "secondary_fixed_dim": "secondary_fixed_dim",
    "shadow": "shadow",
    "surface": "surface",
    "surface_bright": "surface_bright",
    "surface_container": "surface_container",
    "surface_container_high": "surface_container_high",
    "surface_container_highest": "surface_container_highest",
    "surface_container_low": "surface_container_low",
    "surface_container_lowest": "surface_container_lowest",
    "surface_dim": "surface_dim",
    "surface_variant": "surface_variant",
    "tertiary": "tertiary",
    "tertiary_container": "tertiary_container",
    "tertiary_fixed": "tertiary_fixed",
    "tertiary_fixed_dim": "tertiary_fixed_dim",
}


def strip_colormap(whitelist: list = [], blacklist: list = []) -> Dict:
    if not whitelist:
        whitelist = list(DEFAULT_COLORMAP.keys())
    return {
        k: v
        for k, v in DEFAULT_COLORMAP.items()
        if k in whitelist and k not in blacklist
    }


def load_config(app_name: str, config_name: str):
    if validate_app(app_name)["valid"] != "Found and valid":
        querry_app(app_name, validate_app(app_name), False)
        return
    return getattr(
        getattr(
            getattr(
                __import__(f"owo_scripts.apps.{app_name}.{config_name}"),
                "apps",
            ),
            app_name,
        ),
        config_name,
    )


def querry_app(app_name: str, app_overall: dict, status: bool):
    color_header = bcolors.OKBLUE
    color_main = bcolors.OKGREEN
    if not status:
        color_header = bcolors.FAIL
        color_main = bcolors.WARNING
    print(f"{color_header}-=={app_name}==-{bcolors.ENDC}")
    print(f"  - {color_main}Config dir: {app_overall['config_dir']}{bcolors.ENDC}")
    print(f"  - {color_main}Scripts dir: {app_overall['scripts']}{bcolors.ENDC}")
    print(f"  - {color_main}Valid: {app_overall['valid']}{bcolors.ENDC}")
    if not status:
        print(f"{bcolors.OKBLUE}-==Available Apps==-{bcolors.ENDC}")
        print_list(get_apps())
        raise FileNotFoundError


def validate_app(app_name: str) -> dict:
    output = {
        "name": app_name,
        "config_dir": False,
        "scripts": False,
        "valid": "Not found",
    }
    if path.exists(f"{UNITHEME_DIR}/configs/{app_name}"):
        output["config_dir"] = True
    if path.exists(f"{UNITHEME_DIR}/owo_scripts/apps/{app_name}"):
        output["scripts"] = True
    if any([output["config_dir"], output["scripts"]]):
        output["valid"] = "Found but broken"
    if all([output["config_dir"], output["scripts"]]):
        output["valid"] = "Found and valid"
    return output


def print_list(elements: list):
    elements[0] = f"  - {elements[0]}"
    print("\n  - ".join(elements))


def get_apps():
    with open(f"{UNITHEME_DIR}/info.json") as f:
        table = load(f)
    return sorted(
        [
            dir.split("/")[-1]
            for dir in glob(f"{UNITHEME_DIR}/owo_scripts/apps/*")
            if validate_app(dir.split("/")[-1])["valid"] == "Found and valid"
            and dir.split("/")[-1] not in table["blacklist"]
            and (
                dir.split("/")[-1] in table["whitelist"] if table["whitelist"] else True
            )
        ]
    )


def get_themes():
    return sorted(
        [
            dir.split("/")[-1].split(".")[0]
            for dir in glob(f"{UNITHEME_DIR}/colors/*")
            if not "_current" in dir
        ]
    )


def gen_colors(colormap: Dict, opening: List[str], ending: List[str]):
    def decorator(func):
        def wrapper(file: str) -> str:
            def gen_output(table) -> List[str]:
                try:
                    return (
                        opening
                        + [
                            func(colormap_key, table[colormap_value])
                            for colormap_key, colormap_value in colormap.items()
                        ]
                        + ending
                        + ["\n"]
                    )
                except KeyError:
                    raise KeyError(f"Check your colormap:\n{colormap}.")

            with open(file, "r") as f:
                table = load(f)["colors"]
                with open(f"{UNITHEME_DIR}/info.json", "r") as info:
                    variant = load(info)["variant"]
                    return "\n".join(gen_output(table[variant]))

        return wrapper

    return decorator


def force_symlink(file: str, dest_dir: str, dest_file: str) -> int:
    """
    Creates a symbolic link from 'file' to 'dest_dir/dest_file'.

    Args:
        file (str): The source file path.
        dest_dir (str): The destination directory path.
        dest_file (str): The destination file name.

    Returns:
        int: 0 on success, 1 on failure.
    """
    if not path.exists(file):
        print(f"{bcolors.FAIL}No such file: {file}{bcolors.ENDC}")
        print(f"{bcolors.OKBLUE}-==Available properties==-{bcolors.ENDC}")
        return 1

    if not path.exists(dest_dir):
        makedirs(dest_dir)

    if path.exists(f"{dest_dir}/{dest_file}"):
        unlink(f"{dest_dir}/{dest_file}")
    symlink(file, f"{dest_dir}/{dest_file}")
    return 0


def gen_matugen() -> None:
    """
    Generates colors using matugen.
    """
    command = (
        f"matugen image {UNITHEME_DIR}/wallpapers/current/wallpaper.png --json hex >  "
        f"{UNITHEME_DIR}/colors/_current/matugen.json"
    )
    run(command, shell=True)


if __name__ == "__main__":
    print(f"{bcolors.OKBLUE}Config is valid{bcolors.ENDC}")
