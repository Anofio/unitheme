# rofi
def app(args):
    return [
        {
            "colors": ("colors", "rasi"),
            "layouts": ("config", "rasi"),
            "images": ("image", "png"),
            "config_path": "rofi",
            "link_to_config_dir": True,
        },
        {
            "layouts": args["--layout"],
            "images": args["--image"],
        },
        {
            "layouts": "Set with -l",
            "images": "Set with -i",
        },
    ]
