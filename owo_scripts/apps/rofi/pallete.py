from owo_scripts.globals import gen_colors

colormap_default = {
    "background": "surface_container_high",
    "background-alt": "surface_container_highest",
    "foreground": "on_surface",
    "selected": "on_primary",
    "active": "surface_container_high",
    "urgent": "error",
}

opening_default = ["* {"]
ending_default = ["}"]


@gen_colors(colormap_default, opening_default, ending_default)
def generate_default_section(key: str, value: str) -> str:
    return f"    {key}: {value}ff;"


funcs = [generate_default_section]
