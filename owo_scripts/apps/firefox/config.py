# firefox
from json import load

from owo_scripts.globals import UNITHEME_DIR


def app(args):
    with open(f"{UNITHEME_DIR}/info.json") as f:
        table = load(f)
    return (
        {
            "layouts": ("userChrome", "css"),
            "colors": ("colors", "css"),
            "link_to_config_dir": False,
            # "final": f"rm -f ~/.cache/wal/schemes/* && wal -i $UNITHEME_DIR/wallpapers/current/wallpaper.png {"-l" if table["variant"] == 'light' else ""}",
            "finel": f"rm -f ~/.cache/wal/schemes/* && wal -i $UNITHEME_DIR/wallpapers/current/wallpaper.png",
        },
        {
            "layouts": args["--layout"],
        },
        {
            "layouts": "set with -t",
        },
    )
