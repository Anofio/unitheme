from owo_scripts.globals import DEFAULT_COLORMAP, gen_colors

opening_default = [":root {"]
ending_default = ["}"]


@gen_colors(DEFAULT_COLORMAP, opening_default, ending_default)
def generate_firefox_colors(key: str, value: str) -> str:
    return f"  --{key}: {value};"


funcs = [generate_firefox_colors]
