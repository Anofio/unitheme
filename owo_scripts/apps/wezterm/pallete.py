# wezterm
from owo_scripts.globals import gen_colors

colormap = {
    "bg0": "surface",
    "bg1": "surface_container_lowest",
    "bg2": "surface_container_low",
    "fg0": "outline",
    "fg1": "on_surface",
    "fg2": "on_surface_variant",
    "red": "error",
    "orange": "error",
    "yellow": "secondary",
    "green": "tertiary",
    "cyan": "secondary",
    "blue": "primary",
    "purple": "tertiary",
}

opening_header = ["return {"]
ending_header = ["}"]


@gen_colors(colormap, opening_header, ending_header)
def generate_pallete(key: str, value: str) -> str:
    return f"  {key} = '{value}',"


funcs = [generate_pallete]
