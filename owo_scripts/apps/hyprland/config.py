def app(args):
    return [
        {
            "overrides": ("overrides", "conf"),
            "colors": ("borders", "conf"),
            "config_path": "hypr",
            "link_to_config_dir": True,
        },
        {
            "overrides": args["--layout"],
        },
        {
            "overrides": "Set with -l",
        },
    ]
