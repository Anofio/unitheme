from owo_scripts.globals import gen_colors

colormap_default = {
    "col.inactive_border": "on_primary",
    "col.active_border": "primary",
}


opening_default = ["general {"]
ending_default = ["}"]


@gen_colors(colormap_default, opening_default, ending_default)
def generate_border_colors(key: str, value: str) -> str:
    return f"    {key}=0xff{value[1:]}"


funcs = [generate_border_colors]
