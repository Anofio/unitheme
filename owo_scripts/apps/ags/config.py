# ags
def app(args):
    return (
        {
            "layouts": ("config", "js"),
            "imports": ("imports", "js"),
            "styles": ("styles", "dir"),
            "colors": ("styles/colors", "scss"),
            "windows": ("windows", "dir"),
            "widgets": ("widgets", "dir"),
            "assets": ("assets", "dir"),
            "config_path": "ags",
            "link_to_config_dir": True,
            "final": r'variant=$(cat $UNITHEME_DIR/info.json | jq -r ".variant"); '
            r'echo "\$variant: \"$variant\";" > '
            r"$UNITHEME_DIR/configs/ags/current/styles/_variant.scss",
        },
        {
            "layouts": args["--layout"],
            "imports": args["--layout"],
            "styles": args["--layout"],
            "windows": args["--layout"],
            "widgets": args["--layout"],
            "assets": args["--layout"],
        },
        {
            "layouts": "set with -l",
            "imports": "linked",
            "styles": "linked",
            "windows": "linked",
            "widgets": "linked",
            "assets": "linked",
        },
    )
