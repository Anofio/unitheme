from owo_scripts.globals import DEFAULT_COLORMAP, gen_colors

opening = []
ending = []


@gen_colors(DEFAULT_COLORMAP, opening, ending)
def colorgen(key: str, value: str) -> str:
    return f"${key}: {value};"


funcs = [colorgen]
