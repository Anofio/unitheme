from owo_scripts.globals import gen_colors, strip_colormap

colormap_keys = [
    "background",
    "surface_bright",
    "surface_container",
    "surface_container_high",
    "surface_container_highest",
    "surface_container_low",
    "surface_container_lowest",
    "primary",
    "secondary",
    "tertiary",
    "on_surface",
    "outline",
    "outline_variant",
    "error",
]

opening = []
ending = []


@gen_colors(strip_colormap(whitelist=colormap_keys), opening, ending)
def generate_line(key: str, value: str) -> str:
    return f"@define-color {key} {value};"


funcs = [generate_line]
