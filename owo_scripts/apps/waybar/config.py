# waybar
def app(args):
    return (
        {
            "layouts": ("config", "jsonc"),
            "colors": ("colors", "css"),
            "styles": ("style", "css"),
            "config_path": "waybar",
            "final": "if (pidof waybar); then pkill -SIGUSR2 waybar; else (waybar -l 0 &); fi",
            "link_to_config_dir": True,
        },
        {
            "layouts": args["--layout"],
            "styles": args["--layout"],
        },
        {
            "layouts": "set with -l",
            "styles": "linked with layouts",
        },
    )
