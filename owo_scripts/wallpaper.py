from json import dump, load
from os import makedirs
from os import path as os_path
from subprocess import DEVNULL, run

from owo_scripts.globals import (HOME_DIR, UNITHEME_DIR, bcolors,
                                 force_symlink, gen_matugen, get_apps)
from owo_scripts.owoThemeGen import create_colors_for_app

WALLPAPERS_DIR = f"{UNITHEME_DIR}/wallpapers/store"


def main(wallpaper_name: str = "", theme_name: str = "", wp_command: str = ""):
    with open(f"{UNITHEME_DIR}/info.json", "r") as f:
        table_info = load(f)
        if not theme_name:
            theme_name = table_info["theme"]
        if not wallpaper_name:
            wallpaper_name = table_info["wallpaper"]
        wp_command = table_info["wallpaper_command"]

    if theme_name and not any(
        [
            os_path.exists(path)
            for path in (
                "/usr/bin/lutgen",
                f"{HOME_DIR}/.cargo/bin/lutgen",
                f"{HOME_DIR}/.local/share/bin/lutgen",
            )
        ]
    ):

        print(f"{bcolors.FAIL}lutgen not found in $PATH{bcolors.ENDC}")
        return
    if theme_name:

        print(f"Using theme {theme_name} for wallpaper {wallpaper_name}")
        if not os_path.exists(f"{WALLPAPERS_DIR}/{theme_name}"):

            makedirs(f"{WALLPAPERS_DIR}/{theme_name}")
        if not os_path.exists(f"{WALLPAPERS_DIR}/{theme_name}/{wallpaper_name}.png"):

            with open(f"{UNITHEME_DIR}/colors/{theme_name}.json") as f:
                table = load(f)
                print(f"Generating {wallpaper_name} for theme {theme_name}")
                command = (
                    f"lutgen apply {WALLPAPERS_DIR}/originals/{wallpaper_name}.png "
                    + f"-o {WALLPAPERS_DIR}/{theme_name} -- "
                    + f"{" ".join([f'"{value}"' for value in table.values()])}"
                )
                run(
                    command,
                    shell=True,
                    stdout=DEVNULL,
                )
    force_symlink(
        f"{UNITHEME_DIR}/wallpapers/store/{theme_name if theme_name else 'originals'}/{wallpaper_name}.png",
        f"{UNITHEME_DIR}/wallpapers/current",
        "wallpaper.png",
    )
    if wp_command:
        run(wp_command, stdout=DEVNULL, shell=True)
    with open(f"{UNITHEME_DIR}/info.json", "w") as f:
        table_info["wallpaper"] = wallpaper_name
        dump(table_info, f, indent=4)
    print("Regenerating colors..")
    gen_matugen()
    for app in get_apps():
        create_colors_for_app(app, {"--layout": None, "--image": None})
