from json import dump, load

from owo_scripts.globals import UNITHEME_DIR


def main():
    with open(f"{UNITHEME_DIR}/info.json") as info:
        table = load(info)
    if table["variant"] == "dark":
        table["variant"] = "light"
    else:
        table["variant"] = "dark"
    with open(f"{UNITHEME_DIR}/info.json", "w") as info:
        dump(table, info, indent=4)
