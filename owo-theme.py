#!/usr/bin/env python3
"""
owo-theme

Usage:
    owo-theme gen <app> [options]
    owo-theme set <app> [options]
    owo-theme set theme <theme>
    owo-theme toggle
    owo-theme wp
    owo-theme wp <wallpaper_name> [options]
    owo-theme change <dark | light> <theme>
    owo-theme (-a | --apps)

Options:
    -h --help              Show this screen.
    -v --version           Show version.
    -l --layout <layout>   Set layout. (set)
    -i --image <image>     Set image if supported. (set)
    -s --show              Show all available configs. (set | gen | wp)
    -a --apps              Show all available apps.
"""


from docopt import docopt

from owo_scripts.globals import bcolors, get_apps, print_list
from owo_scripts.owoThemeGen import main as gen_theme
from owo_scripts.owoThemeSet import main as set_config
from owo_scripts.toggle import main as toggle_varinat
from owo_scripts.wallpaper import main as set_wallpaper


def main():
    args = docopt(__doc__, version="1.0")

    if args["--apps"]:
        print(f"{bcolors.OKBLUE}-==Available Apps==-{bcolors.ENDC}")
        print_list(get_apps())
        return
    if args["toggle"]:
        toggle_varinat()
        gen_theme({"<app>": "all", "--show": False, "--layout": None, "--image": None})

    if args["wp"]:
        set_wallpaper(args["<wallpaper_name>"] if args["<wallpaper_name>"] else "")

    if args["gen"]:

        if any([args["--layout"], args["--image"]]):
            print(
                f"{bcolors.WARNING}Warning: --layout and --image are ignored in gen mode.{bcolors.ENDC}"
            )

        gen_theme(args)
    elif args["set"]:

        set_config(args)


if __name__ == "__main__":
    main()
